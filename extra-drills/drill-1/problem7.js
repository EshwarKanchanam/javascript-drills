function getNameAndEmailByAge(allIndividuals, age) {
  if (!Array.isArray(allIndividuals)) {
    return "please enter the array details";
  }
  if (isNaN(age)) {
    return "please enter valid age number";
  }

  for (let personDetails of allIndividuals) {
    if (personDetails.age === age) {
      console.log(
        `name is ${personDetails.name} email is ${personDetails.email}`
      );
    }
  }
}

module.exports = getNameAndEmailByAge;
