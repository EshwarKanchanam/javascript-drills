function getHobbiesByAge(students, age) {
  if (!Array.isArray(students)) {
    return "please enter the array details";
  }
  if (isNaN(age)) {
    return "please enter the valid age number";
  }

  for (let student of students) {
    if (student.age == age) {
      console.log(`Hobbies of ${student.name} are ${student.hobbies}`);
    }
  }
}

module.exports = getHobbiesByAge;
