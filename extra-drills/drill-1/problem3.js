function getNamesOfStudentByCountry(peopleDetails, country) {
  if (!Array.isArray(peopleDetails)) {
    return "please enter the array details";
  }
  if (typeof country !== "string") {
    return "please enter the country name as a string";
  }

  let namesOfStudentsByCountry = [];
  for (let person of peopleDetails) {
    if (person.isStudent && person.country === country) {
      namesOfStudentsByCountry.push(person.name);
      console.log(person.name);
    }
  }
  return namesOfStudentsByCountry;
}

module.exports = getNamesOfStudentByCountry;
