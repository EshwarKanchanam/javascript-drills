function getCityAndCountry(allIndividuals) {
  if (!Array.isArray(allIndividuals)) {
    console.log("please enter the array details");
  }

  for (let personDetails of allIndividuals) {
    console.log(
      `${personDetails.name}'s city is ${personDetails.city} and country is ${personDetails.country}.`
    );
  }
}

module.exports = getCityAndCountry;
