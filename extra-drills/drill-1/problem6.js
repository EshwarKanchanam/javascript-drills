function printFirstHobby(allIndividuals) {
  if (!Array.isArray(allIndividuals)) {
    return "please enter the array details";
  }

  for (let personDetails of allIndividuals) {
    let hobbies = personDetails.hobbies;
    if (hobbies.length >= 1) {
      console.log(hobbies[0]);
    }
  }
}

module.exports = printFirstHobby;
