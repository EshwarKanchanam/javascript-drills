function getPersonByIndexPosition(peopleDetails, index) {
  if (
    Array.isArray(peopleDetails) &&
    index >= 0 &&
    index < peopleDetails.length
  ) {
    console.log(
      `person on index ${index}'s name is ${peopleDetails[index].name} and city is ${peopleDetails[index].city}`
    );
  } else {
    console.log("please enter valid index position or data required");
  }
}

module.exports = getPersonByIndexPosition;
