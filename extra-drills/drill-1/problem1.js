function getEmailAddress(students) {
  if (!Array.isArray(students)) {
    return "please enter the array details";
  }

  let allEmailAddresses = [];
  for (let student of students) {
    allEmailAddresses.push(student.email);
  }
  return allEmailAddresses;
}

module.exports = getEmailAddress;
