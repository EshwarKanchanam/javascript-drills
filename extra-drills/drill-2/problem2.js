const isObject = require("./isObject");

function getAllUsersByNationality(users, userNationality) {
  if (!isObject(users)) {
    return "please enter the object details";
  }

  let allUsersWithSameNationlity = [];
  for (let user in users) {
    if (users[user].nationality === userNationality) {
      allUsersWithSameNationlity.push(user);
    }
  }
  return allUsersWithSameNationlity;
}

module.exports = getAllUsersByNationality;
