const isObject = require("./isObject");

function getAllUsersByQualification(users, userQualification) {
  if (!isObject(users)) {
    return "please enter the object details";
  }

  let allUsersWithSameQualification = [];
  for (let user in users) {
    if (users[user].qualification === userQualification) {
      allUsersWithSameQualification.push(user);
    }
  }
  return allUsersWithSameQualification;
}

module.exports = getAllUsersByQualification;
