const isObject = require("./isObject");

function groupUsersByDesignation(users) {
  if (!isObject(users)) {
    return "please enter the object details";
  }

  let groupOfUsersByDesignation = {};
  let allDesignations = getAllDesignations(users);
  let allProgrammingLanguages = getAllProgrammingLanguages(allDesignations);
  for (let programmingLanguage of allProgrammingLanguages) {
    groupOfUsersByDesignation[programmingLanguage] = [];
    for (let user in users) {
      if (users[user].designation.includes(programmingLanguage)) {
        groupOfUsersByDesignation[programmingLanguage] = [
          ...groupOfUsersByDesignation[programmingLanguage],
          user,
        ];
      }
    }
  }
  return groupOfUsersByDesignation;
}

function getAllDesignations(users) {
  let allDesignations = [];
  for (let user in users) {
    allDesignations.push(users[user].designation);
  }
  return allDesignations;
}

function getAllProgrammingLanguages(allDesignations) {
  let allProgrammingLanguages = [];
  for (let designation of allDesignations) {
    let programmingLanguage;
    if (isIntern(designation)) {
      programmingLanguage = designation.split(" ").pop();
    } else {
      let allWordsInDesignation = designation.split(" ");
      if (allWordsInDesignation.length <= 2) {
        programmingLanguage = allWordsInDesignation.at(0);
      } else {
        programmingLanguage = allWordsInDesignation.at(1);
      }
    }
    if (!allProgrammingLanguages.includes(programmingLanguage)) {
      allProgrammingLanguages.push(programmingLanguage);
    }
  }
  return allProgrammingLanguages;
}

function isIntern(designation) {
  if (designation.includes("Intern")) {
    return true;
  }
  return false;
}

module.exports = groupUsersByDesignation;
