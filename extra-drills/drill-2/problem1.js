const isObject = require("./isObject");

function getAllUsersByInterests(users, userInterest) {
  if (!isObject(users)) {
    return "please enter the object details";
  }

  let usersIntrestedIn = [];
  for (let user in users) {
    let allInterests = users[user].interests;

    for (let interest of allInterests) {
      if (interest.toLowerCase().includes(userInterest.toLowerCase())) {
        usersIntrestedIn.push(user);
      }
    }
  }

  return usersIntrestedIn;
}

module.exports = getAllUsersByInterests;
