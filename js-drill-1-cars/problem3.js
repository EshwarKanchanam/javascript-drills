function getCarModelsAlphabetically(inventory) {
  if (!Array.isArray(inventory)) {
    return "please enter the array details";
  }

  let carModels = [];
  for (let car of inventory) {
    carModels.push(car.car_model);
  }
  carModels.sort();
  return carModels;
}

module.exports = getCarModelsAlphabetically;
