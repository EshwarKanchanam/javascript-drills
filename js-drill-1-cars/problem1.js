function getCarDetailsOfId33(inventory) {
  if (!Array.isArray(inventory)) {
    return "please enter the array details";
  }
  if (inventory[33] === undefined) {
    return "car with id 33 is not available";
  }

  return `car 33 is a ${inventory[33].car_year} ${inventory[33].car_make} ${inventory[33].car_model}`;
}

module.exports = getCarDetailsOfId33;
