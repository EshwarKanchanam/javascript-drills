function getAllCarYears(inventory) {
  if (!Array.isArray(inventory)) {
    return "please enter the array details";
  }

  let carYears = [];
  for (let carDetails of inventory) {
    carYears.push(carDetails.car_year);
  }
  return carYears;
}

module.exports = getAllCarYears;
