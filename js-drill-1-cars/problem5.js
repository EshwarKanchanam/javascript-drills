function getAllCarsBefore2000(inventory, carYears) {
  if (!Array.isArray(inventory) && !Array.isArray(carYears)) {
    return "please enter the array details";
  }

  let allCarYearsBefore2000 = getUniqueValues(
    getAllCarYearsBefore(carYears, 2000)
  );
  let allCarsBefore2000 = [];
  for (let carYear of allCarYearsBefore2000) {
    for (let carDetails of inventory) {
      if (carDetails.car_year === carYear) {
        allCarsBefore2000.push(carDetails);
      }
    }
  }
  return allCarsBefore2000;
}

function getAllCarYearsBefore(carYears, year) {
  let allCarYears = [];
  for (let carYear of carYears) {
    if (carYear < year) {
      allCarYears.push(carYear);
    }
  }
  return allCarYears;
}

function getUniqueValues(arrayWithDuplicates) {
  let uniqueValues = [];
  for (let element of arrayWithDuplicates) {
    if (!uniqueValues.includes(element)) {
      uniqueValues.push(element);
    }
  }
  return uniqueValues;
}

module.exports = getAllCarsBefore2000;
