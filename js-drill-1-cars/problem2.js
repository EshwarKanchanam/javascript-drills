function getLastCarDetailsInInventory(inventory) {
  if (!Array.isArray(inventory) && inventory.length === 0) {
    return "please enter the array details";
  }

  const lastCar = inventory.pop();
  return `Last car is a ${lastCar.car_make} ${lastCar.car_model}`;
}

module.exports = getLastCarDetailsInInventory;
