function getBMWAndAudiCars(inventory) {
  if (!Array.isArray(inventory)) {
    return "please enter the array details";
  }

  let bMWandAudiCars = [];
  for (let carDetails of inventory) {
    let carMake = carDetails.car_make;
    if (carMake === "BMW" || carMake === "Audi") {
      bMWandAudiCars.push(carDetails);
    }
  }
  return bMWandAudiCars;
}

module.exports = getBMWAndAudiCars;
