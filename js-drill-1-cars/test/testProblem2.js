const inventory = require('../data');
const getLastCarDetailsInInventory = require('../problem2');

let lastCarDetails = getLastCarDetailsInInventory(inventory);

if(lastCarDetails){
    console.log(lastCarDetails);
}else{
    console.log('inventory is empty');
}