const inventory = require('../data');
const getCarModelsAlphabetically = require('../problem3')

const carModels = getCarModelsAlphabetically(inventory);

if(carModels){
    console.log(carModels);
}else{
    console.log('inventory is empty');
}