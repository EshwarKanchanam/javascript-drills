const inventory = require('../data');
const getAllCarYears = require('../problem4')
const getAllCarsBefore2000 = require('../problem5')

const allCarsBefore2000 = getAllCarsBefore2000(inventory,getAllCarYears(inventory));

if(allCarsBefore2000){
    console.log(allCarsBefore2000.length);
}else{
    console.log('inventory is empty');
}