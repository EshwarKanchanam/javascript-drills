function reduce(elements, callback, startingvalue) {
  let startIndex = 0;

  if (!Array.isArray(elements)) {
    console.log("please enter the array details");
  } else if (startingvalue === undefined) {
    startingvalue = elements[0];
    startIndex = 1;
  }

  for (let index = startIndex; index < elements.length; index++) {
    startingvalue = callback(startingvalue, elements[index], index);
  }
  return startingvalue;
}

module.exports = reduce;
