const items = require('../data')
const reduce = require('../reduce')

let reducerSum = (startingValue, element, index) => startingValue + element;
let sum = reduce(items, reducerSum);
console.log(sum);