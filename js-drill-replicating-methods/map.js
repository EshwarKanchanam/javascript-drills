function map(elements, iteratee) {
  let result = [];

  if (!Array.isArray(elements)) {
    console.log("please enter the array");
  } else {
    for (let index = 0; index < elements.length; index++) {
      result.push(iteratee(elements[index], index));
    }
  }

  return result;
}

module.exports = map;
