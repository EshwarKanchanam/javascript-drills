function filter(elements, callback) {
  let result = [];

  if (!Array.isArray(elements)) {
    console.log("please enter the array details");
  } else {
    for (let index = 0; index < elements.length; index++) {
      if (callback(elements[index], index)) {
        result.push(elements[index]);
      }
    }
  }

  return result;
}

module.exports = filter;
