function flatten(elements) {
  if (!Array.isArray(elements)) {
    console.log("please enter the array details");
  } else {
    return _flatten(elements, []);
  }
}

function _flatten(elements, flattendArr) {
  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      _flatten(elements[index], flattendArr);
    } else {
      flattendArr.push(elements[index]);
    }
  }
  return flattendArr;
}

module.exports = flatten;
