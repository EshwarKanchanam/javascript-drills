function each(elements, callback) {
  if (!Array.isArray(elements)) {
    console.log("please enter the array details");
    return;
  }

  for (let index = 0; index < elements.length; index++) {
    if (elements[index] !== undefined) {
      callback(elements[index], index);
    }
  }
}

module.exports = each;
