function find(elements, callback) {
  if (!Array.isArray(elements)) {
    console.log("please enter the array details");
  } else {
    for (let index = 0; index < elements.length; index++) {
      if (callback(elements[index], index) === true) {
        return elements[index];
      }
    }
  }
  return undefined;
}

module.exports = find;
